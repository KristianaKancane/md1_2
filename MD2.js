import * as THREE from "./three.js-dev/build/three.module.js";
import { StereoEffect } from "./three.js-dev/examples/jsm/effects/StereoEffect.js";
import { OrbitControls } from './three.js-dev/examples/jsm/controls/OrbitControls.js';
import { VRButton } from "./three.js-dev/examples/jsm/webxr/VRButton.js";
import { XRControllerModelFactory } from "./three.js-dev/examples/jsm/webxr/XRControllerModelFactory.js";
import { GUI } from "./three.js-dev/examples/jsm/libs/dat.gui.module.js";


export function md2() {
	const scene = new THREE.Scene();
	const camera = new THREE.PerspectiveCamera(
		75,
		window.innerWidth / window.innerHeight,
		0.1,
		10000
	);
    
	const renderer = new THREE.WebGLRenderer();
	const loader = new THREE.TextureLoader();
	const effect = new StereoEffect(renderer);

	let windowHalfX = window.innerWidth / 2;
	let windowHalfY = window.innerHeight / 2;

	let mouseX = 0;
	let mouseY = 0;
	let is_key_down = false;

	const controls = new OrbitControls( camera, renderer.domElement );

	camera.position.set( 0, 0, 1000 );
	controls.update();
	renderer.setSize(window.innerWidth, window.innerHeight);

	//Geometry
	const geometry = new THREE.BoxGeometry(200, 200, 200);
    const geometry1 = new THREE.BoxGeometry(150, 150, 150);

	//Material
	const material = new THREE.MeshBasicMaterial({
		map: loader.load("./texture2.jpg"),
	});

    const material1 = new THREE.MeshBasicMaterial({
		map: loader.load("./texture3.jpg"),
	});

    let uniform = {
		time: { value: 1 }
	};

    const material2 = new THREE.ShaderMaterial({
		uniforms: uniform,
		vertexShader: document.getElementById('vertexShader').textContent,
		fragmentShader: document.getElementById('fragmentShader').textContent
	});

    const material3 = new THREE.ShaderMaterial({
		uniforms: uniform,
		vertexShader: document.getElementById('vertexShader').textContent,
		fragmentShader: document.getElementById('fragmentShader1').textContent
	});

    const material4 = new THREE.ShaderMaterial({
		uniforms: uniform,
		vertexShader: document.getElementById('vertexShader').textContent,
		fragmentShader: document.getElementById('fragmentShader2').textContent
	});

  	const material5 = new THREE.MeshBasicMaterial({ 
		color: 0xff5479, 
		morphTargets: true 
	});

	//Cube
	const cube = new THREE.Mesh(geometry, material);
	const cube1 = new THREE.Mesh(geometry, material1);
	const cube2 = new THREE.Mesh(geometry1, material4);
    const cube3 = new THREE.Mesh(geometry1, material2);
	const cube4 = new THREE.Mesh(geometry1, material3);

	//Construct 8 blend shapes

	for ( let i = 0; i < 8; i ++ ) {
		const vertices = [];

		for ( let v = 0; v < geometry.vertices.length; v ++ ) {
			vertices.push( geometry.vertices[ v ].clone() );
			
			if ( v === i ) {
				vertices[ vertices.length - 1 ].x *= 2;
				vertices[ vertices.length - 1 ].y *= 2;
				vertices[ vertices.length - 1 ].z *= 2;
			}
		}

		geometry.morphTargets.push( { name: "target" + i, vertices: vertices } );
	}

	const cube5 = new THREE.Mesh( new THREE.BufferGeometry().fromGeometry( geometry ), material5 );
											
	const params = {
		influence1: 0,
		influence2: 0,
		influence3: 0,
		influence4: 0,
		influence5: 0,
		influence6: 0,
		influence7: 0,
		influence8: 0
	};

	const gui = new GUI();

	const folder = gui.addFolder( 'Morph Targets' );
		
	folder.add( params, 'influence1', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 0 ] = value;
	});
		
	folder.add( params, 'influence2', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 1 ] = value;
	});
		
	folder.add( params, 'influence3', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 2 ] = value;
	});
		
	folder.add( params, 'influence4', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 3 ] = value;
	});
				
	folder.add( params, 'influence5', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 4 ] = value;
	});

	folder.add( params, 'influence6', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 5 ] = value;
	});
				
	folder.add( params, 'influence7', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 6 ] = value;
	});
				
	folder.add( params, 'influence8', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		cube5.morphTargetInfluences[ 7 ] = value;
	});

	folder.open();

	cube.position.set(200, 0, 0);
	cube1.position.set(-200, 0, 0);
	cube2.position.set(0, 200, 0);
	cube3.position.set(300, 300, 0);
	cube4.position.set(-300, 300, 0);
    cube5.position.set(0, 0, -500);

	scene.add(cube);
	scene.add(cube1);
    scene.add(cube2);
	scene.add(cube3);
	scene.add(cube4);
	scene.add(cube5);

	renderer.setAnimationLoop(function () {
		cube3.rotation.x += Math.PI / 180;
		cube3.rotation.y += Math.PI / 180;
		cube3.rotation.z += Math.PI / 180;

		cube4.rotation.x += Math.PI / 180;
		cube4.rotation.y += Math.PI / 180;
		cube4.rotation.z += Math.PI / 180;

		uniform['time'].value = performance.now() / 1000;

		controls.update();

		effect.render(scene, camera);
	});

	document.body.appendChild(renderer.domElement);
    document.body.appendChild(VRButton.createButton(renderer));

	//Keyboard
	window.addEventListener('keydown', function (event) {
		let keyCode = event.which;
		let delta = 5;

		if (keyCode == 65) {        //a
			cube2.position.x -= delta;
		}

		if (keyCode == 83) {        //s
			cube2.position.y -= delta;
		}

		if (keyCode == 68) {        //d
			cube2.position.x += delta;
		}

		if (keyCode == 87) {       //w
			cube2.position.y += delta;
		}

		if (keyCode == 189) {       //-
			cube2.position.z -= delta;
		}

		if (keyCode == 187) {       //+
			cube2.position.z += delta;
		}

		is_key_down = true;
	});

	window.addEventListener('keyup', function (event) {
		is_key_down = false;
	});
}

import * as THREE from "./three.js-dev/build/three.module.js";
import { GUI } from "./three.js-dev/examples/jsm/libs/dat.gui.module.js";

export function md1() {
	const gui_container = new GUI();
	const scene = new THREE.Scene();
	const camera = new THREE.PerspectiveCamera(
		75,
		window.innerWidth / window.innerHeight,
		0.1,
		10000
	);

	const vShader = `
		varying vec2 v_uv;

		void main() {
			v_uv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
		}
	`

	const fShader = `
		varying vec2 v_uv;
		uniform vec2 u_mouse;
		uniform vec2 u_resolution;
		uniform vec3 u_color;
		uniform float u_time;

		void main() {
			vec2 v = u_mouse / u_resolution;
			vec2 uv = gl_FragCoord.xy / u_resolution;
			gl_FragColor = vec4(1.0, 0.5, sin(u_time * 10.0) + 0.5, 0.0).rgba;
		}
	`

	//Set clock
	const clock = new THREE.Clock();

	const uniforms = {
		u_mouse: { value: { x: window.innerWidth / 2, y: window.innerHeight / 2 } },
		u_resolution: { value: { x: window.innerWidth, y: window.innerHeight } },
		u_time: { value: 0.0 },
		u_color: { value: new THREE.Color(0xFF0000) }
	}
  
	const renderer = new THREE.WebGLRenderer();
	const loader = new THREE.TextureLoader();
    const directional_light = new THREE.DirectionalLight(0xffcc00, 1);

    directional_light.position.set(0, 0, 10);

	//Geometry
	const geometry = new THREE.BoxGeometry();

    //Material
    const material = new THREE.ShaderMaterial({
		vertexShader: vShader,
		fragmentShader: fShader,
		uniforms
	});

    const material1 = new THREE.MeshPhongMaterial({
		color: 0xa6f74f,
    });

	const material2 = new THREE.MeshPhongMaterial({
		color: 0x47f5ef,
	});

    const material3 = new THREE.MeshPhongMaterial({
		color: 0xfcff33,
	});

    //Cube
    const cube = new THREE.Mesh(geometry, material);
    const cube1 = new THREE.Mesh(geometry, material1);
	const cube2 = new THREE.Mesh(geometry, material2);
    const cube3 = new THREE.Mesh(geometry, material3);

	let mesh;
	let mesh_arr = [];
	let min = 1;
	let max = 12;
	
	for (let this_x = -1; this_x < 2; this_x++) {
		for (let this_y = -1; this_y < 2; this_y++) {
			for (let this_z = -1; this_z < 2; this_z++) {
				var randomnumber = Math.floor(Math.random() * 12) + 1;

					if (randomnumber > 9) {
						mesh = cube.clone();
					} else if (randomnumber > 6){
						mesh = cube1.clone();
					} else if (randomnumber > 3){
						mesh = cube2.clone();
					} else {
						mesh = cube3.clone();
					}
				
				mesh.position.set(this_x * 1.2, this_y * 1.2, this_z * 1.2);
				mesh_arr.push(mesh);
				scene.add(mesh);
			}
		}
	}

    scene.add(directional_light);

	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor( 0x660050 );
	
	let params = {
        twist: 1,
    };

    gui_container.add(params, 'twist', -50, 50).step(1).onChange(function (value) {
        camera.position.y = value;
    });
		
	renderer.setAnimationLoop(function () {
		let rotation_speed = Date.now() * 0.00030;

        camera.position.x = Math.cos(rotation_speed) * 10;
        camera.position.z = Math.sin(rotation_speed) * 10;
        camera.lookAt(scene.position);

        directional_light.position.x = Math.cos(rotation_speed) * 10;
        directional_light.position.z = Math.sin(rotation_speed) * 10;
        directional_light.lookAt(scene.position);
	
		//Rotation
		mesh_arr.forEach(function (mesh_object) {   
		mesh_object.rotation.y += Math.PI / 180;

		//Update time uniform
		uniforms.u_time.value = clock.getElapsedTime();
	});

    renderer.render(scene, camera);
  });

  document.body.appendChild(renderer.domElement);
}
